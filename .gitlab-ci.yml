include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml

# TO DO ! https://gitlab.com/help/user/application_security/dast/index#configuration
# include:
#   template: DAST.gitlab-ci.yml

# TO DO ! https://gitlab.com/help/user/application_security/container_scanning/index
# include:
#   template: Container-Scanning.gitlab-ci.yml

stages:
  - test
  - build
  - release
  - deploy

variables:
  POSTGRES_DB: test_db
  POSTGRES_USER: runner
  POSTGRES_PASSWORD: ""
  POSTGRES_HOST_AUTH_METHOD: trust
  BACKEND_IMAGE: $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_REF_SLUG
  SIDEKIQ_IMAGE: $CI_REGISTRY_IMAGE/sidekiq:$CI_COMMIT_REF_SLUG
  BACKEND_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/backend:latest
  SIDEKIQ_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/sidekiq:latest
  # DAST_WEBSITE: https://jogl-backend-dev.herokuapp.com

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
  - vendor/ruby

.shared_hidden_key: &testing  # This is an anchor
  image: "ruby:2.7.1"
  stage: test
  before_script:
    - ruby -v                                   # Print out ruby version for debugging
    - gem install bundler
    # - RAILS_ENV=test bundle config set path 'vendor'
    - RAILS_ENV=test bundle install -j $(nproc) --path ./vendor  # Install dependencies into ./vendor/ruby
  services:
    - postgres:latest
    - redis:latest
  # Cache gems in between builds
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor

license_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" || $CI_COMMIT_BRANCH != "develop"'
      when: never

rspec:
  <<: *testing
  script:
  - cp config/database.yml.gitlab config/database.yml
  - RAILS_ENV=test bundle exec rails db:migrate
  - RAILS_ENV=test bundle exec rails db:seed
  - RAILS_ENV=test bundle exec rails db:seed:interests
  - RAILS_ENV=test bundle exec rails spec
  artifacts:
    paths:
      - coverage/

.shared_hidden_key: &building  # This is an anchor
  image: "docker:stable"
  stage: build
  services:
    - docker:19.03.5-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - apk add python3 python3-dev py-pip build-base libffi-dev openssl-dev  # curl
    - pip install docker-compose

build:
  <<: *building
  only:
    refs:
      # Deprecating staging for now
      # - staging
      - develop
      - master
  script:
    - docker-compose build web sidekiq
    - docker tag web $BACKEND_IMAGE
    - docker push $BACKEND_IMAGE
    - docker tag sidekiq $SIDEKIQ_IMAGE
    - docker push $SIDEKIQ_IMAGE

release:
  image: "docker:stable"
  stage: release
  services:
    - docker:19.03.5-dind
  only:
    refs:
      - master
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $BACKEND_IMAGE
    - docker pull $SIDEKIQ_IMAGE
    - docker tag $BACKEND_IMAGE $BACKEND_RELEASE_IMAGE
    - docker tag $SIDEKIQ_IMAGE $SIDEKIQ_RELEASE_IMAGE
    - docker push $BACKEND_RELEASE_IMAGE
    - docker push $SIDEKIQ_RELEASE_IMAGE

pages:
  stage: deploy
  dependencies:
    - rspec
  script:
    - mv coverage/ public/
  artifacts:
    paths:
      - public
  only:
    - master

heroku-deploy-develop:
  only:
    refs:
    - develop
  image: "docker:stable"
  services:
  - docker:19.03.5-dind
  stage: deploy
  environment:
    name: develop
  script:
    - sh release.sh

heroku-deploy-staging:
  only:
    refs:
    - staging
  image: "docker:stable"
  services:
  - docker:19.03.5-dind
  stage: deploy
  environment:
    name: staging
  script:
    - sh release.sh
  # Deprecating staging for now
  when: manual

heroku-deploy-master:
  only:
    refs:
    - master
  image: "docker:stable"
  services:
  - docker:19.03.5-dind
  stage: deploy
  environment:
    name: master
  script:
    - sh release.sh

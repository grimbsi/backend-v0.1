class CreateBoards < ActiveRecord::Migration[5.2]
  def change
    create_table :boards do |t|
      t.string :title
      t.string :description
      t.bigint :boardable_id
      t.string :boardable_type
      t.timestamps
    end

    create_table :users_boards do |t|
      t.bigint :user_id
      t.bigint :board_id
      t.string :role
      t.timestamps
    end
  end
end

# frozen_string_literal: true

class AddShortBioToUserObj < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :short_bio, :string
  end
end

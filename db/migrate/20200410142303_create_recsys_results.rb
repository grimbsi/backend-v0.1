# frozen_string_literal: true

class CreateRecsysResults < ActiveRecord::Migration[5.2]
  def change
    create_table :recsys_results do |t|
      t.string :sourceable_node_type
      t.bigint :sourceable_node_id
      t.string :targetable_node_type
      t.bigint :targetable_node_id
      t.string :relation_type
      t.float :value

      t.timestamps
    end
  end
end

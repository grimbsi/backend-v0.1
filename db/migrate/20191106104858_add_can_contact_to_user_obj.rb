# frozen_string_literal: true

class AddCanContactToUserObj < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :can_contact, :bool
  end
end

# frozen_string_literal: true

class CreateJoinTableCommunitiesUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users_communities do |t|
      t.belongs_to :user, index: true
      t.belongs_to :community, index: true
      t.string :role
    end
  end
end

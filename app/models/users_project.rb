# frozen_string_literal: true

class UsersProject < ApplicationRecord
  belongs_to :user
  belongs_to :project
  # after_update :notify_admin

  # private
  # def notify_admin
  # 	if status_changed? && status == "accepted"
  #     NotificationEmailWorker.perform_async(project.creator_id, "Your request for joining the project has been accepted", 'project/' + @project.id.to_s', "You have been accepted to join a project!")
  # 	end
  # end
end

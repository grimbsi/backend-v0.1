# frozen_string_literal: true

class Notification < NotificationHandler::Notification
  include Notifications

  after_create :push_notifications

  scope :unread, -> { where(read: false) }

  # Settings Hash Schema

  # IMPORTANT: This is a hashie:mash object, so to set the values the only way I found was
  # user.settings.categories!.send("#{category}!").enabled = false <- This will set the category to false for a whole category
  # user.settings.delivery_methods!.email!.enabled = false  <- this will stop pushing using the delivery method :email

  # target.settings =
  # {
  #   enabled: true/false <- This sets the Notification master switch, if false, notifications for the users won't be created at all
  #   delivery_methods: {   <- this is the master switch for a notification setting
  #     method: {    <- This is the delivery method set in the notification-pusher config.register_delivery_method
  #       enabled: true/false   <- This is the flag
  #     }
  #   }
  #   categories: {  <- The categories are set in the notification-settings initializer
  #     category: {   <- This is the category from the notification category
  #       enabled: true/false <- This allows/prevents the creation of notification on that category
  #       delivery_methods: { <- we can fine grain the notifications settings here by changin which delivery method works
  #         method: { <- this is the method that is set in the notification-pusher config.register_delivery_method
  #           enabled: true/false  <- Same same
  #         }
  #       }
  #     }
  #   }
  # }

  # This function here handles all the custom delivery methods
  def push_notifications
    deliver(:email)
    # We can add more methods later on like push notifications on apps
  end

  def read!
    update(read: true)
  end

  def deliver_email
    NotificationEmailWorker.perform_async(id)
    # NotificationMailer.notify(self).deliver # temp code to make initial testing easier
  end

  def subject_line
    args = {
      scope: 'notifications',
      default: "PLEASE DEFINE a translation for #{I18n.locale}.notifications.#{type}",
      object_type: object.class.to_s.downcase
    }
    # communities are called "groups" on the frontend
    args[:object_type] = 'group' if object.class == Community
    if metadata.key?(:author_id)
      author = User.find(metadata[:author_id])
      args.merge!(author_first_name: author.first_name)
      args.merge!(author_last_name: author.last_name)
    end
    args.merge!(object_title: object.title) if object.respond_to?(:title)
    args.merge!(project_title: object.project.title) if object.respond_to?(:project)
    # ick
    if object.present? && object.respond_to?(:feed) && object&.feed&.feedable&.class&.name != 'User'
      args.merge!(feedable_title: object&.feed&.feedable&.title)
      args.merge!(feedable_type: object&.feed&.feedable&.class&.name&.downcase)
    else
      args.merge!(feedable_title: '-DELETED-')
      args.merge!(feedable_type: '-DELETED-')
    end

    I18n.t(type, **args)
  end

  def link
    # TODO
    # Here we need to add checks for the type of notificiation !
    # For example, pending_project should send the link to the admin page of the challenge
    # And most (all?) administration notification should link to the settings of the object.

    # account for object being deleted
    object&.frontend_link
  end
end

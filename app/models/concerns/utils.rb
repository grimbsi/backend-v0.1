# frozen_string_literal: true

module Utils
  extend ActiveSupport::Concern

  def make_address
    [address, city, country].compact.join(', ')
  end

  def obj_type
    self.class.name
  end

  def sanitize(text)
    ActionController::Base.helpers.sanitize(
      text,
      options = {
        tags: %w[p br h1 h2 h3 a strong em u s sub sup span img iframe pre blockquote ul li ol],
        attributes: %w[height width style src href rel target class allowfullscreen frameborder data-value data-index data-denotation-char data-link]
      }
    )
  end

  def sanitize_description
    self.description = sanitize(description)
  end

  def sanitize_content
    self.content = sanitize(content)
  end

  def banner_url_sm
    return banner_url unless banner.attachment.present?

    variant = banner.variant(resize: '100x100^')
    Rails.application.routes.url_helpers.rails_representation_url(variant)
  end

  def members_count
    User.with_role(:member, self).count
  end

  def users_sm
    # Should return the first 6 members of a object ranked by their roles in the project
    # Owners first, then admins, then members.

    # NOTE:
    # feels bad that with_any_role returns an array and not a chainable ActiveRecord query object
    User
      .with_any_role({ name: :owner, resource: self },
                     { name: :admin,  resource: self },
                     { name: :member, resource: self })
      .uniq
      .first(6)
      .map do |user|
      {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        short_bio: user.short_bio,
        owner: user.has_role?(:owner, self),
        admin: user.has_role?(:admin, self),
        member: user.has_role?(:member, self),
        logo_url: user.logo_url_sm
      }
    end
  end

  def update_skills(new_skills)
    new_skills = clean_array(new_skills)
    old_skills = skills.collect(&:skill_name)
    # Using set logic to get the new items
    skills_to_add = new_skills - old_skills
    unless skills_to_add.empty?
      skills_to_add.each do |skill_name|
        skill = Skill.find_or_create_by!(skill_name: skill_name)
        skills << skill
        add_edge(skill, 'has_skill')
      end
    end
    # Using set logic to get the items that don't exist anymore
    skills_to_remove = old_skills - new_skills
    unless skills_to_remove.empty?
      skills_to_remove.each do |skill_name|
        skill = Skill.find_by(skill_name: skill_name)
        skills.delete(skill)
        remove_edge(skill, 'has_skill')
      end
    end
  end

  def update_ressources(new_ressources)
    new_ressources = clean_array(new_ressources)
    old_ressources = ressources.collect(&:ressource_name)
    # Using set logic to get the new items
    ressources_to_add = new_ressources - old_ressources
    unless ressources_to_add.empty?
      ressources_to_add.each do |ressource_name|
        ressource = Ressource.find_or_create_by!(ressource_name: ressource_name)
        ressources << ressource
        add_edge(ressource, 'has_ressource')
      end
    end

    # Using set logic to get the items that don't exist anymore
    ressources_to_remove = old_ressources - new_ressources
    unless ressources_to_remove.empty?
      ressources_to_remove.each do |ressource_name|
        ressource = Ressource.find_by(ressource_name: ressource_name)
        ressources.delete(ressource)
        remove_edge(ressource, 'has_ressource')
      end
    end
  end

  def update_interests(new_interest_ids)
    # no need to clean interests array as it's a fixed set of integers
    current_interest_ids = interests.pluck(:id)
    # Using set logic to get the new items
    ids_of_interests_to_add = new_interest_ids - current_interest_ids
    Interest.where(id: ids_of_interests_to_add).find_each do |interest|
      interests << interest
      add_edge(interest, 'has_interest')
    end
    # Using set logic to get the items that don't exist anymore
    ids_of_interests_to_remove = current_interest_ids - new_interest_ids
    Interest.where(id: ids_of_interests_to_remove).find_each do |interest|
      interests.delete(interest)
      remove_edge(interest, 'has_interest')
    end
  end

  # NOTE: called as part of callbacks, e.g.
  # has_many :users, through: :users_programs, after_add: :reindex, after_remove: :reindex
  # it passes the added user, but we don't use it
  def reindex(_user)
    index! if persisted?
  end

  def clean_string(string)
    string.singularize.downcase.capitalize.strip
  end

  def clean_array(array)
    result = array.map do |el|
      el.split(',').map do |subel|
        clean_string(subel)
      end
    end
    result.flatten
  end
end

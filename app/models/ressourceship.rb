# frozen_string_literal: true

class Ressourceship < ApplicationRecord
  belongs_to :ressourceable, polymorphic: true
  belongs_to :ressource
end

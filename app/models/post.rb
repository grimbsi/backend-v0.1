# frozen_string_literal: true

class Post < ApplicationRecord
  # Each post must have a content must belong to user, must belong to a feed.
  # Document restrictions are applied in order to not let a user upload anything
  resourcify
  notification_object
  include AlgoliaSearch
  include RelationHelpers
  include Utils
  include RecsysHelpers

  belongs_to :user

  # Yes, it's weird that a post both
  # `belongs_to :feed` and `:has_and_belongs_to_many :feeds`
  # the first is really the source feed, and the habtm relationship
  # are the cross-postings
  belongs_to :feed

  has_and_belongs_to_many :feeds

  # From seems to be the 'feedable' object from the feed, denormalized onto this table
  belongs_to :from, polymorphic: true, foreign_type: :from_object, foreign_key: :from_id

  has_many :comments
  has_many :mentions
  has_many_attached :documents

  validates :content, presence: true

  before_create :sanitize_content
  before_update :sanitize_content

  # TODO: SERIOUSLY FIX THOSE
  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb

  algoliasearch disable_indexing: !Rails.env.production? do
    use_serializer Api::PostSerializer
  end

  def creator
    {
      id: user.id,
      first_name: user.first_name,
      last_name: user.last_name,
      logo_url: user.logo_url_sm
    }
  end

  def users
    [user, comments.map(&:user)].flatten.uniq
  end

  def participants_except_commenter(comment)
    # user_id(post creator) + all_other_commenters - current_commenter
    participant_ids = (([user_id] + comments.pluck(:user_id)).uniq - [comment.user_id])
    User.where(id: participant_ids)
  end

  def frontend_link
    "/post/#{id}"
  end

  def notif_new_clap(author)
    Notification.create(
      target: user,
      category: :clap,
      type: 'new_clap',
      object: self,
      metadata: { author_id: author.id }
    )
  end
end

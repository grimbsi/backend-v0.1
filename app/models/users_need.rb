# frozen_string_literal: true

class UsersNeed < ApplicationRecord
  belongs_to :user
  belongs_to :need
end

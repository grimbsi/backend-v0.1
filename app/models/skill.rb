# frozen_string_literal: true

class Skill < ApplicationRecord
  include AlgoliaSearch

  ###### FOR RECOMANDER SYSTEM ############
  include RecsysHelpers
  belongs_to :sourceable, polymorphic: true, optional: true
  belongs_to :targetable, polymorphic: true, optional: true
  #########################################

  has_and_belongs_to_many :users
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :community
  has_and_belongs_to_many :challenge
  has_and_belongs_to_many :needs

  algoliasearch disable_indexing: Rails.env.test? do
    attribute :skill_name
  end
end

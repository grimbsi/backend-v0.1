# frozen_string_literal: true

class Api::CommunitiesController < ApplicationController
  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :authenticate_user!, only: %i[create update destroy upload my_communities]
  before_action :find_community, except: %i[create index my_communities short_title_exist get_id_from_name recommended]
  before_action :set_obj, except: %i[index create short_title_exist get_id_from_name recommended]
  before_action :is_admin, only: %i[update invite upload update_member remove_member]

  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Utils
  include Api::ExternalLinks
  include Api::Recommendations

  def index
    # take order param in the url, and order array depending on its value
    param = if params[:order] == 'desc'
              'id DESC'
            else
              'id ASC'
            end
    @pagy, @communities = pagy(Community.where.not(status: 'archived').order(param).all)
    render json: @communities
  end

  def my_communities
    @communities = Community.with_role(:owner, current_user)
    @communities += Community.with_role(:admin, current_user)
    @communities += Community.with_role(:member, current_user)
    @pagy, @communities = pagy_array(@communities.uniq)
    render json: @communities
  end

  # On creation the creator get all roles, skills and interests are added manually
  def create
    render(json: { data: 'ShortTitle is already taken' }, status: :unprocessable_entity) && return unless Community.where(short_title: params[:community][:short_title]).empty?

    @community = Community.new(community_params)
    @community.creator_id = current_user.id
    @community.status = 'active'
    @community.users << current_user
    if @community.save
      current_user.add_edge(@community, 'is_author_of')
      @community.update_skills(params[:community][:skills]) unless params[:community][:skills].nil?
      @community.update_ressources(params[:community][:ressources]) unless params[:community][:ressources].nil?
      @community.update_interests(params[:community][:interests]) unless params[:community][:interests].nil?
      current_user.add_role :owner, @community
      current_user.add_role :admin, @community
      current_user.add_role :member, @community
      current_user.add_edge(@community, 'is_member_of')
      current_user.add_edge(@community, 'is_admin_of')
      current_user.add_edge(@community, 'is_owner_of')
      render json: { id: @community.id, data: 'Success' }, status: :created
    end
  end

  def show
    render json: @community, show_objects: true
  end

  def update
    unless params[:community][:creator_id].nil?
      render json: { data: 'You cannot change the creator_id' }, status: :forbidden if (@community.creator_id != params[:community][:creator_id]) && (@community.creator_id != current_user.id)
    end
    if @community.update_attributes(community_params)
      @community.update_skills(params[:community][:skills]) unless params[:community][:skills].nil?
      @community.update_ressources(params[:community][:ressources]) unless params[:community][:ressources].nil?
      @community.update_interests(params[:community][:interests]) unless params[:community][:interests].nil?
      json_response(@community)
    else
      render json: { data: 'Something went wrong !' }, status: :unprocessable_entity
    end
  end

  def destroy
    if @community.users.count == 1
      @community.destroy
      current_user.remove_edge(@community, 'is_author_of')
      render json: { data: 'Community destroyed' }, status: :ok
    else
      @community.update_attributes({ status: 'archived' })
      render json: { data: 'Community archived' }, status: :ok
    end
  end

  private

  def find_community
    @community = Community.find(params[:id])
    render json: { data: "Community id:#{params[:id]} not found" }, status: :not_found if @community.nil?
  end

  def set_obj
    @obj = @community
  end

  def community_params
    params.require(:community).permit(:title, :short_title, :description, :short_description,
                                      :country, :city, :address, :is_private, :creator_id,
                                      :stranger_id, :current_user_id, :ressources)
  end
end

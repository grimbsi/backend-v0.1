# frozen_string_literal: true

class Api::ProgramsController < ApplicationController
  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :authenticate_user!, only: %i[create update destroy my_programs]
  before_action :find_program, except: %i[index create my_programs get_id_from_name short_title_exist recommended]
  before_action :find_challenge, only: %i[attach remove]
  before_action :set_obj, except: %i[index create destroy short_title_exist get_id_from_name recommended]
  before_action :is_admin, only: %i[update invite upload_banner remove_banner
                                    update_member remove_member]
  before_action :can_create, only: [:create]

  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Utils
  include Api::Recommendations
  include Api::Faq
  include Api::ExternalLinks
  include Api::Resources

  def index
    @pagy, @programs = pagy(Program.where.not(status: 'draft').includes([:banner_attachment, :avatar_attachment, :feed]).all)
    render json: @programs
  end

  def my_programs
    @programs = Program.with_role(:owner, current_user)
    @programs += Program.with_role(:admin, current_user)
    @programs += Program.with_role(:member, current_user)
    @pagy, @programs = pagy_array(@programs.uniq)
    render json: @programs
  end

  def create
    @program = Program.new(program_params)
    @program.status = 'draft'
    @program.users << current_user
    if @program.save
      @program.create_feed
      @program.create_faq
      current_user.add_role :owner, @program
      current_user.add_role :admin, @program
      current_user.add_role :member, @program
      render json: { id: @program.id, data: 'Success' }, status: :created
    end
  end

  def show
    render json: @program, show_objects: true
  end

  def getid
    @program = Program.where(short_title: params[:nickname]).first
    render json: { id: @program.id, data: 'Success' }, status: :ok
  end

  def update
    if @program.update_attributes(program_params)
      render json: @program, status: :ok
    else
      render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
    end
  end

  def destroy
    # why count the users in a program?
    # this would be more logical @program.users.delete_all followed by status modification
    # Leo: Because if the program has only one user we consider it a "test" aka you can ACTUALLY deleted it
    # Else, it stays as archived
    if @program.users.count == 1
      @program.destroy
      render json: { data: "Program id:#{@program.id} destroyed" }, status: :ok
    else
      @program.update_attributes({ status: 'archived' })
      render json: { data: "Program id:#{@program.id} archived" }, status: :ok
    end
  end

  # This method add challenge to a program and changes its status
  def attach
    if @program.challenges.include?(@challenge)
      render json: { data: "Challenge id:#{@challenge.id} is already attached" }, status: :ok
    else
      @program.challenges << @challenge
      render json: { data: "Challenge id:#{@challenge.id} attached" }, status: :ok
    end
  end

  # The object relation is remove no status change
  def remove
    if @program.challenges.include?(@challenge)
      @program.challenges.delete(@challenge)
      render json: { data: "Challenge id:#{@challenge.id} removed" }, status: :ok
    else
      render json: { data: "Challenge id:#{@challenge.id} is not attached" }, status: :not_found
    end
  end

  def index_challenges
    render json: @program, serializer: Api::ProgramChallengeSerializer
  end

  def index_projects
    render json: Api::ProgramProjectSerializer.new(context: { current_user: current_user }).serialize_to_json(@program)
  end

  def index_needs
    render json: Api::ProgramNeedSerializer.new(context: { current_user: current_user }).serialize_to_json(@program)
  end


  private

  def find_program
    @program = Program.find_by(id: params[:id])
    render json: { data: "Program id:#{ params[:id] } not found" }, status: :not_found if @program.nil?
  end

  def find_challenge
    @challenge = Challenge.find_by(id: params[:challenge_id])
    render json: { data: "Challenge id:#{ params[:challenge_id] } was not found" }, status: :not_found if @challenge.nil?
  end

  def set_obj
    @obj = @program
  end

  def can_create
    render json: { data: 'You do not have the right to create a program' }, status: :forbidden unless current_user.has_role? :program_creator
  end

  def program_params
    params.require(:program).permit(:title, :title_fr, :short_title, :short_title_fr, :short_description, :short_description_fr, :description, :description_fr, :status, :launch_date, :end_date, :enablers, :enablers_fr, :ressources, :contact_email, :meeting_information)
  end
end

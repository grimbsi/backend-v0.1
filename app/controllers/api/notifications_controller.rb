# frozen_string_literal: true

class Api::NotificationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_notification, only: %i[read unread show]
  before_action :set_notifications, only: %i[index all_read all_unread]
  before_action :is_self, only: %i[read unread show]

  def index
    @pagy, @mynotifications = pagy(@notifications)
    render json: @mynotifications, status: :ok
  end

  def show
    render json: @notification, status: :ok
  end

  def read
    @notification.read = true
    @notification.save
    render json: { data: "Notification #{params[:id]} has been read" }, status: :ok
  end

  def all_read
    @notifications.update_all(read: true)
    render json: { data: 'Notifications have been read' }, status: :ok
  end

  def unread
    @notification.read = false
    @notification.save
    render json: { data: "Notification #{params[:id]} has been unread" }, status: :ok
  end

  def all_unread
    @notifications.update_all(read: false)
    render json: { data: 'Notifications have been unread' }, status: :ok
  end

  def get_settings
    settings = {
      enabled: default_value(current_user.settings.enabled),
      categories: {},
      delivery_methods: {}
    }
    NotificationSettings.configuration.categories.each do |category|
      settings[:categories][category] = {}
      settings[:categories][category][:enabled] = default_value(current_user.settings.categories!.send("#{category}!").enabled)
      settings[:categories][category][:delivery_methods] = {}
      NotificationPusher.configuration.delivery_methods.each do |delivery_method, _|
        settings[:categories][category][:delivery_methods][delivery_method] = {}
        settings[:categories][category][:delivery_methods][delivery_method][:enabled] = default_value(current_user.settings.categories!.send("#{category}!").delivery_methods!.send("#{delivery_method}!").enabled)
      end
    end
    NotificationPusher.configuration.delivery_methods.each do |delivery_method, _|
      settings[:delivery_methods][delivery_method] = {}
      settings[:delivery_methods][delivery_method][:enabled] = default_value(current_user.settings.delivery_methods!.send("#{delivery_method}!").enabled)
    end
    render json: {settings: settings}, status: :ok
  end

  def set_settings
    settings = params[:settings]
    current_user.settings.enabled = booleanify(settings[:enabled]) unless settings[:enabled].nil?
    unless settings[:categories].nil?
      settings[:categories].each do |category, value|
        current_user.settings.categories!.send("#{category}!").enabled = booleanify(settings[:categories][category][:enabled])
        settings[:categories][category][:delivery_methods].each do |delivery_method, value|
          current_user.settings.categories!.send("#{category}!").delivery_methods!.send("#{delivery_method}!").enabled = booleanify(settings[:categories][category][:delivery_methods][delivery_method][:enabled])
        end
      end
    end
    unless settings[:delivery_methods].nil?
      settings[:delivery_methods].each do |delivery_method, value|
        current_user.settings.delivery_methods!.send("#{delivery_method}!").enabled = booleanify(settings[:delivery_methods][delivery_method][:enabled])
      end
    end
    if current_user.save
      render json: {data: "Settings saved"}, status: :ok
    else
      render json: {data: "Something went wrong"}, status: :unprocessable_entity
    end
  end

  # def subscribe
  #   @notification.read!
  #   render json: {data: "Notification " + params[:id].to_s + " has been read"}, status: :ok
  # end
  #
  # def unsubscribe
  #   @notification.unread!
  #   render json: {data: "Notification " + params[:id].to_s + " has been unread"}, status: :ok
  # end

  private

  def booleanify(value)
    ActiveModel::Type::Boolean.new.cast(value)
  end

  def default_value(value)
    if value.nil?
      true
    else
      value
    end
  end

  def set_notification
    @notification = Notification.find_by(id: params[:id])
    render(json: { data: "Cannot find notification with id: #{params[:id]}" }, status: :not_found) && return if @notification.nil?
  end

  def set_notifications
    @notifications = current_user.notifications
  end

  def is_self
    render(json: { data: "You cannot look at someone else's notifications!" }, status: :forbidden) && return unless current_user == @notification.target
  end
end

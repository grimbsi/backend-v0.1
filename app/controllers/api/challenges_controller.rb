# frozen_string_literal: true

class Api::ChallengesController < ApplicationController
  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :authenticate_user!, only: %i[create my_challenges can_create update destroy attach remove set_project_status]
  before_action :find_challenge, except: %i[index create my_challenges can_create short_title_exist get_id_from_name recommended]
  before_action :find_project, only: %i[attach remove]
  before_action :find_project_relation, only: [:set_project_status]
  before_action :set_obj, except: %i[index create my_challenges can_create short_title_exist get_id_from_name recommended]
  before_action :is_admin, only: %i[update invite upload update_member remove_member
                                    create_external_hook update_external_hook delete_external_hook get_external_hooks]

  # Including methods
  include Api::ExternalLinks
  include Api::Faq
  include Api::Follow
  include Api::Hooks
  include Api::Members
  include Api::Recommendations
  include Api::Relations
  include Api::Upload
  include Api::Utils

  def index
    @pagy, @challenges = pagy(Challenge.where.not(status: 'draft').all)
    render json: @challenges
  end

  def my_challenges
    @challenges = Challenge.with_role(:owner, current_user)
    @challenges += Challenge.with_role(:admin, current_user)
    @challenges += Challenge.with_role(:member, current_user)
    @pagy, @challenges = pagy_array(@challenges.uniq)
    render json: @challenges
  end

  def can_create
    if current_user.has_role? :challenge_creator
      render json: { data: 'Authorized' }, status: :ok
    else
      render json: { data: 'Forbidden' }, status: :forbidden
    end
  end

  def create
    render(json: { data: 'Forbidden' }, status: :forbidden) && return unless current_user.has_role? :challenge_creator

    @challenge = Challenge.new(challenge_params)
    @challenge.users << current_user
    if @challenge.save
      current_user.add_edge(@challenge, 'is_author_of')
      @challenge.update_skills(params[:challenge][:skills]) unless params[:challenge][:skills].nil?
      @challenge.update_interests(params[:challenge][:interests]) unless params[:challenge][:interests].nil?
      current_user.add_role :owner, @challenge
      current_user.add_role :admin, @challenge
      current_user.add_role :member, @challenge
      current_user.add_edge(@challenge, 'is_member_of')
      current_user.add_edge(@challenge, 'is_admin_of')
      current_user.add_edge(@challenge, 'is_owner_of')
      render json: { id: @challenge.id, short_title: @challenge.short_title, data: 'Challenge created succefully' }, status: :created
    else
      render json: { data: 'Something went wrong' }, status: :unprocessable_entity
    end
  end

  def show
    render json: @challenge, show_objects: true
  end

  def update
    render(json: { data: 'You are not an admin of this challenge' }, status: :forbidden) && return unless current_user.has_role? :admin, @challenge

    if @challenge.update_attributes(challenge_params)
      @challenge.update_skills(params[:challenge][:skills]) unless params[:challenge][:skills].nil?
      @challenge.update_interests(params[:challenge][:interests]) unless params[:challenge][:interests].nil?
      # @challenge.geocode('challenge')
      render json: { data: "Challenge id:#{@challenge.id} has been updated" }, status: :ok
    else
      render json: { data: 'Something went wrong' }, status: :unprocessable_entity
    end
  end

  # This method add project to a challenge and changes its status
  def attach
    if @challenge.projects.include?(@project)
      render json: { data: "Project id:#{@project.id} is already attached" }, status: :ok
    else
      @challenge.projects << @project
      @project.add_edge(@challenge, 'is_part_of')
      find_project_relation
      @relation.project_status = 'pending'
      @relation.save!
      hook_new_project_challenge(@challenge.externalhooks, @project)
      render json: { data: "Project id:#{@project.id} attached" }, status: :ok
    end
  end

  # The object relation is removed with no status change
  def remove
    if @challenge.projects.include?(@project)
      @challenge.projects.delete(@project)
      @project.remove_edge(@challenge, 'is_part_of')
      render json: { data: "Project id:#{@project.id} removed" }, status: :ok
    else
      render json: { data: "Project id:#{@project.id} is not attached" }, status: :not_found
    end
  end

  def index_projects
    render json: @challenge, serializer: Api::ChallengeProjectSerializer
  end

  def index_needs
    render json: @challenge, serializer: Api::ChallengeNeedSerializer
  end

  def set_project_status
    if params[:status]
      if params[:status] == "accepted"
        @relation.accepted!
        @challenge.add_users_from_projects
        @challenge.program&.add_user_from_challenges
        render json: { data: 'Status changed' }, status: :ok
      elsif params[:status] == "pending"
        @relation.pending!
      else
        render json: { data: "Status invalid. Accepted values are: 'accepted' or 'pending'" }, status: :unprocessable_entity
      end
    else
      render json: { data: 'You need to provide a status in the json body' }
    end
  end

  def destroy
    if @challenge.users.count == 1
      @challenge.destroy
      current_user.remove_edge(@challenge, 'is_author_of')
      current_user.remove_edge(@challenge, 'is_member_of')
      current_user.remove_edge(@challenge, 'is_admin_of')
      current_user.remove_edge(@challenge, 'is_owner_of')
      render json: { data: "Challenge id:#{params[:id]} destroyed" }, status: :ok
    else
      @challenge.update_attributes({ status: 'archived' })
      render json: { data: "Challenge id:#{params[:id]} archived" }, status: :ok
    end
  end

  private

  def find_challenge
    @challenge = Challenge.find(params[:id])
    render json: { data: "Challenge id:#{params[:id]} was not found" }, status: :not_found if @challenge.nil?
  end

  def find_project_relation
    @relation = ChallengesProject.find_by(challenge_id: params[:id], project_id: params[:project_id])
    render json: { data: 'Challenge Project Relation not found' }, status: :not_found if @relation.nil?
  end

  def set_obj
    @obj = @challenge
  end

  def find_project
    @project = Project.find(params[:project_id])
    render json: { data: "Project id:#{params[:project_id]} was not found" }, status: :not_found if @project.nil?
  end

  def challenge_params
    params.require(:challenge).permit(:title, :title_fr, :description, :description_fr, :short_description, :short_description_fr, :rules, :rules_fr, :faq, :faq_fr, :program_id, :country,
                                      :city, :address, :status, :launch_date, :end_date, :final_date, :skills, :interests, :short_title)
  end
end

# frozen_string_literal: true

# TODO: test that mail is sent
# todo: internationalization

# TODO: bell notification on frontend
#
class NotificationMailer < ApplicationMailer
  def notify(notification)
    @target = notification.target
    @object = notification.object
    @author = User.find(notification.metadata[:author_id]) if notification.metadata.key?(:author_id)
    @followed_thing = @object.respond_to?(:feed) && @object.feed.feedable
    @notification = notification
    mail(
      to: "<#{@target.email}>",
      subject: notification.subject_line,
      template_path: 'notifications',
      template_name: notification.type
    )
  end
end

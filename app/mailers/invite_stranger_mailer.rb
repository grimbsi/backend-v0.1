# frozen_string_literal: true

class InviteStrangerMailer < ApplicationMailer
  def invite_stranger(owner, object, stranger)
    @owner = owner
    @object = object
    @from_object = object.class.name.downcase != 'community' ? object.class.name.downcase : 'group'
    @stranger = stranger
    mail(to: "<#{stranger}>", subject: 'You have been invited to join a ' + @from_object)
  end
end

# frozen_string_literal: true

require 'digest/sha2'
class DeviseMailer < Devise::Mailer
  default 'message-id' => "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@jogl.io>"
  default from: 'JOGL - Just One Giant Lab <noreply@jogl.io>'
end

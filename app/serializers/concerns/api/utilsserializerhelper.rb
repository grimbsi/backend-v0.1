# frozen_string_literal: true

module Api::Utilsserializerhelper
  def documents
    if object.documents.attached?
      object.documents.attachments.map do |document|
        url = Rails.application.routes.url_helpers.rails_blob_url(document)
        if document.variable?
          variant = document.variant(resize: '400x400^')
          url_variant = Rails.application.routes.url_helpers.rails_representation_url(variant)
        else
          url_variant = url
        end
        {
          id: document.blob.id,
          content_type: document.blob.content_type,
          filename: document.blob.filename,
          url_variant: url_variant,
          url: url
        }
      end
    else
      []
    end
  end

  def documents_feed
    unless object.feed.nil?
      attachments = get_feed_attachments(object)
      attachments.flatten
    end
  end

  def get_feed_attachments(obj)
    obj.feed.posts.map do |post|
      next unless post.documents.attached?

      post.documents.attachments.map do |document|
        url = Rails.application.routes.url_helpers.rails_blob_url(document)
        if document.variable?
          variant = document.variant(resize: '400x400^')
          url_variant = Rails.application.routes.url_helpers.rails_representation_url(variant)
        else
          url_variant = url
        end
        {
          id: document.blob.id,
          content_type: document.blob.content_type,
          filename: document.blob.filename,
          url_variant: url_variant,
          url: url
        }
      end
    end
  end

  def geoloc(obj = nil)
    obj = object if obj.nil?
    {
      lat: obj.latitude,
      lng: obj.longitude
    }
  end

  def interests(obj = nil)
    obj = object if obj.nil?
    obj.interests.map(&:id)
  end

  def skills(obj = nil)
    obj = object if obj.nil?
    obj.skills.map(&:skill_name)
  end

  def ressources(obj = nil)
    obj = object if obj.nil?
    obj.ressources.map(&:ressource_name)
  end

  def badges(obj = nil)
    obj = object if obj.nil?
    if obj.class.method_defined? :badges
      obj.badges.map do |badge|
        {
          id: badge.id,
          name: badge.name,
          description: badge.description,
          custom_fields: badge.custom_fields
        }
      end
    else
      []
    end
  end

  def feed_id
    object.feed.id
  end

  def scope?
    defined?(current_user).nil?
  end
end

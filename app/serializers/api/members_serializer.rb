# frozen_string_literal: true

class Api::MembersSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes  :id,
              :first_name,
              :last_name,
              :nickname,
              :status,
              :skills,
              :ressources,
              :interests,
              :can_contact,
              :current_sign_in_at,
              :projects_count,
              :mutual_count,
              :logo_url,
              :logo_url_sm,
              :short_bio,
              :owner,
              :admin,
              :member,
              :pending,
              :has_followed,
              :has_clapped,
              :has_saved,
              :geoloc

  def owner
    object.has_role?(:owner, @instance_options[:parent])
  end

  def admin
    object.has_role?(:admin, @instance_options[:parent])
  end

  def member
    object.has_role?(:member, @instance_options[:parent])
  end

  def pending
    object.has_role?(:pending, @instance_options[:parent])
  end

  def mutual_count
    if current_user.nil?
      0
    else
      object.follow_mutual_count(current_user)
    end
  end
end

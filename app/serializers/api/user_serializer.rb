# frozen_string_literal: true

class Api::UserSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Utilsserializerhelper

  attributes :active_status,
             :affiliation,
             :age,
             :badges,
             :bio,
             :birth_date,
             :can_contact,
             :category,
             :city,
             :claps_count,
             :confirmed_at,
             :country,
             :current_sign_in_at,
             :feed_id,
             :first_name,
             :follower_count,
             :following_count,
             :gender,
             :id,
             :interests,
             :last_name,
             :logo_url_sm,
             :logo_url,
             :mail_newsletter,
             :mail_weekly,
             :needs_count,
             :nickname,
             :projects_count,
             :ressources,
             :saves_count,
             :short_bio,
             :skills,
             :status

  attribute :email_notifications_enabled, unless: :exclude_email_notifications_enabled?
  attribute :geoloc, unless: :scope?
  attribute :has_clapped, unless: :scope?
  attribute :has_followed, unless: :scope?
  attribute :has_saved, unless: :scope?
  attribute :is_admin, unless: :scope?

  def scope?
    defined?(current_user).nil?
  end

  def exclude_email_notifications_enabled?
    !defined?(instance_options[:exclude_email_notifications_enabled]).nil?
  end
end

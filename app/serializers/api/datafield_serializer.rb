# frozen_string_literal: true

class Api::DatafieldSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :title,
             :description,
             :format,
             :unit,
             :created_at,
             :updated_at
end

# frozen_string_literal: true

class Types::NotificationsWithCountsConnection < GraphQL::Types::Relay::BaseConnection
  edge_type(NotificationEdgeType)

  field :total_count, Integer, null: true
  def total_count
    # - `object` is the Connection
    # - `object.items` is the original collection of Notifications
    object.items.size
  end

  field :unread_count, Integer, null: true
  def unread_count
    object.items.unread.size
  end
end

# frozen_string_literal: true

class RecsysWorker
  include Sidekiq::Worker

  def perform
    system('python3 -m jogl.recsys')
  end
end

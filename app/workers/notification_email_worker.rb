# frozen_string_literal: true

class NotificationEmailWorker
  include Sidekiq::Worker

  def perform(notification_id)
    notification = Notification.find(notification_id)
    NotificationMailer.notify(notification).deliver
  end
end

# frozen_string_literal: true

class InviteStrangerEmailWorker
  include Sidekiq::Worker

  def perform(user_id, object_type, object_id, joiner_email)
    @owner = User.find(user_id)
    @object = object_type.constantize.find(object_id)
    InviteStrangerMailer.invite_stranger(@owner, @object, joiner_email).deliver
  end
end

# frozen_string_literal: true

class NeedAdminError < ApiExceptions::BaseException
  def initialize(message: 'Only the creator of the need or an admin can update it', status: :forbidden)
    @data = message
    @status = status
  end
end

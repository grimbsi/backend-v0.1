# frozen_string_literal: true

class AccessForbiddenError < ApiExceptions::BaseException
  def initialize(message: 'Forbidden', status: :forbidden)
    @data = message
    @status = status
  end
end

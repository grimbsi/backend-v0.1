# frozen_string_literal: true

FactoryBot.define do
  factory :program do
    description { 'MyString' }
    title { FFaker::Movie.unique.title }
    short_title { FFaker::Internet.unique.user_name }
    short_description { FFaker::Movie.title }

    after :create do |program|
      user = create(:confirmed_user)
      program.users << user
      user.add_role :member, program
      user.add_role :admin, program
      user.add_role :owner, program

      program.create_faq
      program.create_feed
    end
  end
end

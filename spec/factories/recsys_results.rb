# frozen_string_literal: true

FactoryBot.define do
  factory :recsys_result do
    source_node_type { 'MyString' }
    source_node_id { '' }
    target_node_type { 'MyString' }
    target_node_id { '' }
    relation_type { 'MyString' }
    value { 1.5 }
  end
end

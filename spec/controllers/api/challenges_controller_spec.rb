# frozen_string_literal: true

require 'rails_helper'
require_relative 'clappable_shared_example'
require_relative 'followable_shared_example'
require_relative 'saveable_shared_example'
require_relative 'membership_shared_example'
require_relative 'recommendations_shared_example'
require_relative 'linkable_shared_example'
require_relative 'faqable_shared_example'

RSpec.describe Api::ChallengesController, type: :controller do
  context 'Examples' do
    describe 'challenge relations' do
      it_behaves_like 'an object with clappable', :challenge
      it_behaves_like 'an object with followable', :challenge
      it_behaves_like 'an object with saveable', :challenge
    end

    describe 'challenge mixins' do
      it_behaves_like 'an object with members', :challenge
      it_behaves_like 'an object with an faq', :challenge
      it_behaves_like 'an object with recommendations', :challenge
      it_behaves_like 'an object with links', :challenge
    end
  end

  describe 'object with sub object members' do
    describe '#members_list' do
      before do
        @user = create(:confirmed_user, first_name: 'Megan', last_name: 'Strant')
        @challenge = create(:challenge)
        @user.add_role :admin, @challenge
        sign_in @user
        @user2 = create(:confirmed_user)
        @user2.add_role :member, @challenge
      end

      it "should get the members of the #{described_class}, and including owners/admins/members/pending_members of the project" do
        project = create(:project) # note: adds creator as a member
        @challenge.projects << project
        user3 = create(:confirmed_user, first_name: 'Roberta')
        user3.add_role(:member, project)
        user4 = create(:confirmed_user, first_name: 'Michael')
        user4.add_role(:pending, project)
        user5 = create(:confirmed_user, first_name: 'Owner')
        user5.add_role(:owner, project)
        user6 = create(:confirmed_user, first_name: 'Admin')
        user6.add_role(:admin, project)

        get :members_list, params: { id: @challenge.id }

        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['members'].count).to eq(8)
        expect(json_response['members'].map { |m| m['first_name'] }).to include('Roberta', 'Michael', 'Owner', 'Admin')
      end

      it "should not return users not members of the #{described_class}" do
        @challenge.users << create(:confirmed_user)
        get :members_list, params: { id: @challenge.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['members'].count).to eq 3
      end

      it "should paginate the members of the #{described_class}" do
        get :members_list, params: { id: @challenge.id, items: 1, page: 1 }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['members'].count).to eq 1
      end

      # TODO: figure out efficient ordering of user by object role
      xit 'should order the returned users correctly' do
        get :members_list, params: { id: @challenge.id, f: true }
        json_response = JSON.parse(response.body)['members']
        expect(json_response.count).to eq(3)

        expect(json_response[0]['owner']).to eq(true)
        expect(json_response[1]['admin']).to eq(true)
        expect(json_response[2]['member']).to eq(true)
      end
    end
  end

  describe '#index_needs' do
    context 'we can get the list of needs attached to a program' do
      it 'should return the list of needs from projects accepted to the challenge' do
        megan = create(:confirmed_user, first_name: 'Megan')
        challenge = create(:challenge)
        # attach some needs to an accepted project
        project1 = create(:project, creator: megan)
        challenge.projects << project1
        project1.needs << create(:need, project: project1, user: megan)
        project1.needs << create(:need, project: project1, user: megan)
        challenge.challenges_projects.update_all(project_status: :accepted)

        # attach some needs to a non accepted project
        project2 = create(:project, creator: megan)
        challenge.projects << project2
        project2.needs << create(:need, project: project2, user: megan)
        project2.needs << create(:need, project: project2, user: megan)

        get :index_needs, params: { id: challenge.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response['needs'].count).to eq 2
      end
    end
  end

  context 'Challenge admin' do
    before(:each) do
      @user = create(:confirmed_user)
      sign_in @user
      @challenge = create(:challenge)
      @challenge.users << @user
      @user.add_role :member, @challenge
      @user.add_role :admin, @challenge
      @user.add_role :owner, @challenge
    end

    describe 'can manage projects' do
      before(:each) do
        @project = create(:project)
        @challenge.projects << @project
        @challenge.challenges_projects.find_by(project: @project).pending!
      end

      it 'and change the status' do
        post :set_project_status, params: { id: @challenge.id, project_id: @project.id, status: 'accepted' }
        JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@challenge.challenges_projects.find_by(project_id: @project.id).accepted?).to be_truthy
      end

      it 'and can remove a project' do
        delete :remove, params: { id: @challenge.id, project_id: @project.id }
        JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@challenge.projects.count).to eq 0
      end
    end
  end

  context 'Project owner' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @challenge = create(:challenge)
      @project = create(:project, creator_id: @user.id)
    end

    it 'can attach its project' do
      put :attach, params: { id: @challenge.id, project_id: @project.id }
      JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@challenge.projects.count).to eq 1
      expect(@challenge.challenges_projects.find_by(project_id: @project.id).pending?).to be_truthy
      expect(RecsysDatum.where(sourceable_node_type: @project.class.name, sourceable_node_id: @project.id, targetable_node_type: @challenge.class.name, targetable_node_id: @challenge.id, relation_type: 'is_part_of')).to exist
    end

    it 'attach to challenge triggers a notification' do
      expect do
        put :attach, params: { id: @challenge.id, project_id: @project.id }
      end.to change(Notification, :count).by(1)
      notif = Notification.where(object_type: @project.class.name, object_id: @project.id, type: 'pending_project').first
      expect(notif).to be_truthy
      expect(notif.category).to match('administration')
      expect(notif.target).to be == @challenge.users.first
      expect(notif.metadata[:challenge_id]).to be(@challenge.id)
    end

    it 'can remove its project' do
      @challenge.projects << @project
      delete :remove, params: { id: @challenge.id, project_id: @project.id }
      JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@challenge.projects.count).to eq 0
      expect(RecsysDatum.where(sourceable_node_type: @project.class.name, sourceable_node_id: @project.id, targetable_node_type: @challenge.class.name, targetable_node_id: @challenge.id, relation_type: 'is_part_of')).not_to exist
    end

    describe 'can change a challenge status' do
      before do
        @user.add_role :member, @challenge
        @user.add_role :admin, @challenge
        @challenge.users << @user
      end

      it "from 'draft' to 'soon' which triggers notifications" do
        patch :update, params: { id: @challenge.id, challenge: { status: 'soon' } }
        @challenge.reload
        notif = Notification.where(object_type: @challenge.class.name, object_id: @challenge.id, type: 'soon_challenge')
        expect(notif.count).to eq(@challenge.users.count)
        expect(notif.first.category).to match('program')
      end

      it "from 'soon' to 'active' which triggers notifications" do
        @challenge.soon!
        patch :update, params: { id: @challenge.id, challenge: { status: 'active' } }
        @challenge.reload
        notif = Notification.where(object_type: @challenge.class.name, object_id: @challenge.id, type: 'start_challenge')
        expect(notif.count).to eq(User.all.count)
        expect(notif.first.category).to match('program')
      end

      it "from 'active' to 'completed' without a program" do
        @challenge.active!
        patch :update, params: { id: @challenge.id, challenge: { status: 'completed' } }
        @challenge.reload
        notif = Notification.where(object_type: @challenge.class.name, object_id: @challenge.id, type: 'end_challenge')
        expect(notif.count).to be(@challenge.users.count)
        expect(notif.first.category).to match('program')
        expect(notif.first.metadata[:program_id]).to be_falsey
      end

      it "from 'active' to 'completed' in a program" do
        @challenge.active!
        @program = create(:program)
        @program.users << @user
        @user.add_role :member, @program
        @program.challenges << @challenge
        patch :update, params: { id: @challenge.id, challenge: { status: 'completed' } }
        @challenge.reload
        notif = Notification.where(object_type: @challenge.class.name, object_id: @challenge.id, type: 'end_challenge')
        expect(notif.count).to be(@challenge.users.count + @challenge.program.users.count)
        expect(notif.first.category).to match('program')
        expect(notif.first.metadata[:program_id]).to be(@program.id)
      end
    end

    describe 'except if it is not an admin' do
      before do
        @user.add_role :member, @challenge
        @challenge.users << @user
      end

      it 'if not admin' do
        patch :update, params: { id: @challenge.id, challenge: { status: 'completed' } }
        expect(response.status).to eq 403
      end
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::NotificationsController, type: :controller do
  context 'User is logged in' do
    before do
      @user = create(:confirmed_user)
      @otheruser = create(:confirmed_user)
      sign_in @user
      @project = create(:project, creator_id: @user.id)
      @project.notif_new_follower(@otheruser)
      @project.notif_new_member(@otheruser)
    end

    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}
        expect(response).to be_successful
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq(2)
      end
    end

    describe 'GET #show' do
      it 'returns a success response' do
        notification = Notification.first
        get :show, params: { id: notification.id }
        expect(response).to be_successful
        json_response = JSON.parse(response.body)
        expect(json_response['target_type']).to match('User')
        expect(json_response['target_id']).to eq(@user.id)
        expect(json_response['object_type']).to match('Project')
        expect(json_response['object_id']).to eq(@project.id)
      end
    end

    describe 'GET #settings' do
      it 'returns the user settings with default to true' do
        get :get_settings, params: {}
        expect(response).to be_successful
        json_response = JSON.parse(response.body)
        expect(json_response['settings']['enabled']).to be_truthy
        expect(json_response['settings']['categories']['notification']['enabled']).to be_truthy
        expect(json_response['settings']['categories']['notification']['delivery_methods']['email']['enabled']).to be_truthy
        expect(json_response['settings']['delivery_methods']['email']['enabled']).to be_truthy
      end
    end

    describe 'POST #settings' do
      it 'sets the user settings' do
        post :set_settings, params: { settings: {
          enabled: false,
          categories: {
            notification: {
              enabled: false,
              delivery_methods: {
                email: {
                  enabled: true
                }
              }
            }
          }
        } }
        expect(response).to be_successful
        @user.reload
        expect(@user.settings.enabled).to be_falsey
        expect(@user.settings.categories.notification.enabled).to be_falsey
        expect(@user.settings.categories.notification.delivery_methods.email.enabled).to be_truthy
      end
    end

    describe 'PUT #read' do
      it 'returns a success response' do
        notification = Notification.first
        put :read, params: { id: notification.id }
        expect(response).to be_successful
        notification.reload
        expect(notification.read?).to be_truthy
      end
    end

    describe 'PUT #unread' do
      it 'returns a success response' do
        notification = Notification.first
        notification.read = true
        notification.save
        expect(notification.read?).to be_truthy
        put :unread, params: { id: notification.id }
        expect(response).to be_successful
        notification.reload
        expect(notification.read?).to be_falsey
      end
    end

    describe 'PUT #readall' do
      it 'returns a success response' do
        put :all_read, params: {}
        expect(response).to be_successful
        notifications = Notification.where(target_type: 'User', target_id: @user.id)
        notifications.each do |notification|
          expect(notification.read?).to be_truthy
        end
      end
    end

    describe 'PUT #unreadall' do
      it 'returns a success response' do
        notifications = Notification.where(target_type: 'User', target_id: @user.id)
        notifications.update_all(read: true)
        put :all_unread, params: {}
        expect(response).to be_successful
        notifications = Notification.where(target_type: 'User', target_id: @user.id)
        notifications.each do |notification|
          expect(notification.read?).to be_falsey
        end
      end
    end
  end

  context 'User is logged in and settings is set to not trigger follow notifications' do
    before do
      @user = create(:confirmed_user)
      @user.settings.enabled = true
      @user.settings.categories!.follow!.enabled = false
      @user.save!
      @otheruser = create(:confirmed_user)
      sign_in @user
      @project = create(:project, creator_id: @user.id)
      @project.notif_new_follower(@otheruser)
      @project.notif_new_member(@otheruser)
    end

    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}
        expect(response).to be_successful
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq(1)
        expect(json_response.first['type']).to match('new_member')
      end
    end
  end

  context 'User is logged in and settings is set to not trigger membership notifications' do
    before do
      @user = create(:confirmed_user)
      @user.settings.enabled = true
      @user.settings.categories!.membership!.enabled = false
      @user.save!
      @otheruser = create(:confirmed_user)
      sign_in @user
      @project = create(:project, creator_id: @user.id)
      @project.notif_new_follower(@otheruser)
      @project.notif_new_member(@otheruser)
    end

    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}
        expect(response).to be_successful
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq(1)
        expect(json_response.first['type']).to match('new_follower')
      end
    end
  end

  context 'User is logged in and has notifications disabled' do
    before do
      @user = create(:confirmed_user)
      @user.settings.enabled = false
      @user.save!
      @otheruser = create(:confirmed_user)
      sign_in @user
      @project = create(:project, creator_id: @user.id)
      @project.notif_new_follower(@otheruser)
      @project.notif_new_member(@otheruser)
    end

    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}
        expect(response).to be_successful
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq(0)
      end
    end
  end

  context 'User is NOT logged in' do
    before do
      @user = create(:confirmed_user)
      @otheruser = create(:confirmed_user)
      @project = create(:project, creator_id: @user.id)
      @project.notif_new_follower(@otheruser)
      @project.notif_new_member(@otheruser)
    end

    describe 'GET #index' do
      it 'returns a success response' do
        get :index, params: {}
        expect(response.status).to eq 401
      end
    end

    describe 'GET #show' do
      it 'returns a success response' do
        notification = Notification.first
        get :show, params: { id: notification.id }
        expect(response.status).to eq 401
      end
    end

    describe 'GET #settings' do
      it 'returns the user settings with default to true' do
        get :get_settings, params: {}
        expect(response.status).to eq 401
      end
    end

    describe 'POST #settings' do
      it 'sets the user settings' do
        post :set_settings, params: { settings: {
          enabled: false,
          categories: {
            notification: {
              enabled: false,
              delivery_methods: {
                email: {
                  enabled: true
                }
              }
            }
          }
        } }
        expect(response.status).to eq 401
      end
    end

    describe 'PUT #read' do
      it 'returns a success response' do
        notification = Notification.first
        put :read, params: { id: notification.id }
        expect(response.status).to eq 401
        notification.reload
        expect(notification.read?).to be_falsey
      end
    end

    describe 'PUT #unread' do
      it 'returns a success response' do
        notification = Notification.first
        notification.read = true
        notification.save
        expect(notification.read?).to be_truthy
        put :unread, params: { id: notification.id }
        expect(response.status).to eq 401
        notification.reload
        expect(notification.read?).to be_truthy
      end
    end

    describe 'PUT #readall' do
      it 'returns a success response' do
        put :all_read, params: {}
        expect(response.status).to eq 401
        notifications = Notification.where(target_type: 'User', target_id: @user.id)
        notifications.each do |notification|
          expect(notification.read?).to be_falsey
        end
      end
    end

    describe 'PUT #unreadall' do
      it 'returns a success response' do
        notifications = Notification.where(target_type: 'User', target_id: @user.id)
        notifications.update_all(read: true)
        put :all_unread, params: {}
        expect(response.status).to eq 401
        notifications = Notification.where(target_type: 'User', target_id: @user.id)
        notifications.each do |notification|
          expect(notification.read?).to be_truthy
        end
      end
    end
  end

  context 'User is logged in but asking for someone else notification' do
    before do
      @user = create(:confirmed_user)
      @otheruser = create(:confirmed_user)
      sign_in @otheruser
      @project = create(:project, creator_id: @user.id)
      @project.notif_new_follower(@otheruser)
      @project.notif_new_member(@otheruser)
    end

    describe 'GET #show' do
      it 'returns a success response' do
        notification = Notification.first
        get :show, params: { id: notification.id }
        expect(response.status).to eq 403
      end
    end

    describe 'PUT #read' do
      it 'returns a success response' do
        notification = Notification.first
        put :read, params: { id: notification.id }
        expect(response.status).to eq 403
        notification.reload
        expect(notification.read?).to be_falsey
      end
    end

    describe 'PUT #unread' do
      it 'returns a success response' do
        notification = Notification.first
        notification.read = true
        notification.save
        expect(notification.read?).to be_truthy
        put :unread, params: { id: notification.id }
        expect(response.status).to eq 403
        notification.reload
        expect(notification.read?).to be_truthy
      end
    end
  end
end

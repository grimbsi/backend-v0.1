# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with resources' do |factory|
  let(:resource) { build(:document).attributes }

  before do
    @object = create(factory)
  end

  context 'Signed in and admin' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @user.add_role :admin, @object
    end

    it 'GET #index_resource shows the resource' do
      resources = create_list(:document, 3)
      resources.each { |resource| @object.resources << resource }

      get :index_resource, params: { id: @object.id }

      expect(response).to have_http_status(:ok)
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 3
      expect(json_response.pluck('id')).to include(*resources.map(&:id))
    end

    it 'POST #add_resource creates the resource' do
      expect do
        post :add_resource, params: { id: @object.id, resource: build(:document).attributes }
      end.to change(@object.resources, :count).by(1)
      expect(response).to have_http_status(:created)
    end

    it 'PATCH #update_resource updates the resource' do
      @resource = create(:document)
      @object.resources << @resource
      patch :update_resource, params: { id: @object.id, resource_id: @resource.id, resource: resource }
      expect(response).to have_http_status :created
      @resource.reload
      expect(@resource.title).to eq resource['title']
    end

    it 'DELETE #remove_resource deletes the resource' do
      @resource = create(:document)
      @object.resources << @resource
      expect do
        delete :remove_resource, params: { id: @object.id, resource_id: @resource.id }
      end.to change(@object.resources, :count).by(-1)
      expect(response).to have_http_status(:ok)
    end
  end

  context 'Signed in and Not Admin' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    it 'GET #index_resource shows the resource' do
      resources = create_list(:document, 3)
      resources.each { |resource| @object.resources << resource }

      get :index_resource, params: { id: @object.id }

      expect(response).to have_http_status(:ok)
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 3
      expect(json_response.pluck('id')).to include(*resources.map(&:id))
    end

    it 'POST #add_resource does not creates the resource' do
      expect do
        post :add_resource, params: { id: @object.id, resource: build(:document).attributes }
      end.to change(@object.resources, :count).by(0)
      expect(response).to have_http_status(:forbidden)
    end

    it 'PATCH #update_resource does NOT update the resource' do
      @resource = create(:document)
      @object.resources << @resource
      patch :update_resource, params: { id: @object.id, resource_id: @resource.id, resource: resource }
      expect(response).to have_http_status(:forbidden)
      @resource.reload
      expect(@resource.title).not_to eq resource['title']
    end

    it 'DELETE #remove_resource does not deletes the resource' do
      @resource = create(:document)
      @object.resources << @resource
      expect do
        delete :remove_resource, params: { id: @object.id, resource_id: @resource.id }
      end.to change(@object.resources, :count).by(0)
      expect(response).to have_http_status(:forbidden)
    end
  end

  context 'Not Signed in' do
    it 'GET #index_resource shows the resource' do
      resources = create_list(:document, 3)
      resources.each { |resource| @object.resources << resource }

      get :index_resource, params: { id: @object.id }

      expect(response).to have_http_status(:ok)
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 3
      expect(json_response.pluck('id')).to include(*resources.map(&:id))
    end

    it 'POST #add_resource does not creates the resource' do
      expect do
        post :add_resource, params: { id: @object.id, resource: build(:document).attributes }
      end.to change(@object.resources, :count).by(0)
      expect(response).to have_http_status(:unauthorized)
    end

    it 'PATCH #update_resource does NOT update the resource' do
      @resource = create(:document)
      @object.resources << @resource
      patch :update_resource, params: { id: @object.id, resource_id: @resource.id, resource: resource }
      expect(response).to have_http_status(:unauthorized)
      @resource.reload
      expect(@resource.title).not_to eq resource['title']
    end

    it 'DELETE #remove_resource does not creates the resource' do
      @resource = create(:document)
      @object.resources << @resource
      expect do
        delete :remove_resource, params: { id: @object.id, resource_id: @resource.id }
      end.to change(@object.resources, :count).by(0)
      expect(response).to have_http_status(:unauthorized)
    end
  end
end

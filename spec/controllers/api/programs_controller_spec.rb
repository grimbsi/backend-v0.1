# frozen_string_literal: true

require 'rails_helper'
require_relative 'clappable_shared_example'
require_relative 'followable_shared_example'
require_relative 'saveable_shared_example'
require_relative 'membership_shared_example'
require_relative 'recommendations_shared_example'
require_relative 'faqable_shared_example'
require_relative 'linkable_shared_example'
require_relative 'resourceable_shared_example'

RSpec.describe Api::ProgramsController, type: :controller do
  context 'Shared Examples' do
    describe 'program relations' do
      it_behaves_like 'an object with clappable', :program
      it_behaves_like 'an object with followable', :program
      it_behaves_like 'an object with saveable', :program
    end

    describe 'Program' do
      it_behaves_like 'an object with recommendations', :program
      it_behaves_like 'an object with an faq', :program
      it_behaves_like 'an object with links', :program
      it_behaves_like 'an object with resources', :program
      it_behaves_like 'an object with members', :program
    end
  end

  before do
    @program = create(:program)
  end

  context 'Controller specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe '#index' do
      it 'should return list of program' do
        get :index
        expect(response).to have_http_status :ok
      end
    end

    describe '#my_programs' do
      context 'getting users programs with logged in user' do
        it 'should return list of users programs' do
          get :my_programs
          expect(response).to have_http_status :ok
        end
      end

      context 'getting users programs without logged in user' do
        it 'should return list of users programs' do
          sign_out @user
          get :my_programs
          expect(response).to have_http_status :unauthorized
        end
      end
    end

    describe '#create' do
      context 'without logged in user' do
        it "Return Unauthorized because user's not signed in" do
          sign_out @user
          post :create, params: { program: @program.attributes }
          expect(response.status).to eq 401
        end
      end

      context 'creating program with unapproved user' do
        it 'returns forbidden' do
          @program.short_title = FFaker::Name.first_name
          post :create, params: { program: @program.attributes }
          expect(response.status).to eq 403
        end
      end

      context 'creating program with approved user' do
        it 'creates a program' do
          program = build(:program)
          @user.add_role :program_creator
          post :create, params: { program: program.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(json_response['data']).to eq 'Success'
        end
      end
    end

    describe '#show' do
      it 'should return program' do
        get :show, params: { id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Program', targetable_node_id: json_response['id'], relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      context 'Updating program' do
        it 'Updates a program' do
          @user.add_role :admin, @program
          put :update, params: { program: { description: 'New description' }, id: @program.id }
          expect(response.status).to eq 200
        end

        it "from 'draft' to 'soon' status which triggers notifications" do
          @user.add_role :member, @program
          @user.add_role :admin, @program
          @program.users << @user
          expect do
            put :update, params: { program: { status: 'soon' }, id: @program.id }
          end.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          notif = Notification.where(object_type: @program.class.name, object_id: @program.id, type: 'soon_program')
          expect(notif.count).to eq(User.count)
          expect(notif.first.category).to match('program')
        end

        it "from 'soon' to 'active' status which triggers notifications" do
          @program.soon!
          @user.add_role :member, @program
          @user.add_role :admin, @program
          @program.users << @user
          expect do
            put :update, params: { program: { status: 'active' }, id: @program.id }
          end.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          notif = Notification.where(object_type: @program.class.name, object_id: @program.id, type: 'start_program')
          expect(notif.count).to eq(User.count)
          expect(notif.first.category).to match('program')
        end

        it "from 'active' to 'completed' status which triggers notifications" do
          @program.active!
          @user.add_role :member, @program
          @user.add_role :admin, @program
          @program.users << @user
          expect do
            put :update, params: { program: { status: 'completed' }, id: @program.id }
          end.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          notif = Notification.where(object_type: @program.class.name, object_id: @program.id, type: 'end_program')
          expect(notif.count).to eq(User.count)
          expect(notif.first.category).to match('program')
        end
      end

      context 'User not logged in' do
        it "Should throw Unauthorized error and won't update the program" do
          sign_out @user
          put :update, params: { program: { description: 'New description' }, id: @program.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#destroy' do
      context 'User not logged in' do
        it 'Should Not Delete the program' do
          sign_out @user
          delete :destroy, params: { id: @program.id }
          expect(response.status).to eq 401
        end
      end

      context 'User not logged in' do
        it 'Should Not Delete the program' do
          delete :destroy, params: { id: @program.id }
          expect(response.status).to eq 200
        end
      end
    end

    describe '#attach' do
      context 'attaching a challenge to program with logged in user' do
        it 'should attach a challenge to a program' do
          challenge = create(:challenge)
          put :attach, params: { id: @program.id, challenge_id: challenge.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq "Challenge id:#{challenge.id} attached"
        end
      end
    end

    describe '#get_challenges' do
      context 'we can get the list of challenges attached to a program' do
        it 'should return the list of challenges' do
          challenge1 = create(:challenge)
          challenge2 = create(:challenge)
          @program.challenges << challenge1 << challenge2
          get :index_challenges, params: { id: @program.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['challenges'].count).to eq 2
        end
      end
    end

    describe '#get_projects' do
      context 'we can get the list of projects attached to a program' do
        it 'should return the list of projects' do
          challenge1 = create(:challenge)
          challenge2 = create(:challenge)
          project1 = create(:project, creator_id: @user.id)
          project2 = create(:project, creator_id: @user.id)
          project3 = create(:project, creator_id: @user.id)

          challenge1.projects << project1 << project2
          @program.challenges << challenge1 << challenge2
          challenge2.projects << project3 << project1
          get :index_projects, params: { id: @program.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['projects'].count).to eq 3
        end
      end
    end

    describe '#index_needs' do
      context 'we can get the list of needs attached to a program' do
        it 'should return the list of needs' do
          challenge1 = create(:challenge)
          challenge2 = create(:challenge)
          project1 = create(:project, creator_id: @user.id)
          project2 = create(:project, creator_id: @user.id)
          project3 = create(:project, creator_id: @user.id)
          project1.needs << create(:need, project_id: project1.id, user_id: @user.id)
          project1.needs << create(:need, project_id: project1.id, user_id: @user.id)
          project2.needs << create(:need, project_id: project2.id, user_id: @user.id)
          project3.needs << create(:need, project_id: project3.id, user_id: @user.id)
          challenge1.projects << project1 << project2
          challenge1.challenges_projects.update_all(project_status: :accepted)
          @program.challenges << challenge1 << challenge2
          challenge2.projects << project1 << project3
          challenge2.challenges_projects.update_all(project_status: :accepted)
          get :index_needs, params: { id: @program.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['needs'].count).to eq 4
        end
      end
    end

    describe '#remove' do
      context 'removing a challenge from program with logged in user' do
        it 'should remove a challenge from a program' do
          challenge = create(:challenge)
          @program.challenges << challenge
          delete :remove, params: { id: @program.id, challenge_id: challenge.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq "Challenge id:#{challenge.id} removed"
        end
      end
    end

    describe "Banner Upload" do
      it 'should allow a PNG image' do
        @user.add_role :admin, @program
        png = fixture_file_upload('files/authorized.png', 'image/png')
        post :upload_banner, params: { id: @program.id, banner: png }
        expect(response).to be_success
      end

      it 'should not allow a TIFF image' do
        @user.add_role :admin, @program
        tiff = fixture_file_upload('files/unauthorized.tiff', 'image/tiff')
        post :upload_banner, params: { id: @program.id, banner: tiff }
        expect(response.status).to eq(422)
      end
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Board, type: :model do
  describe 'validation' do
    it 'should have valid factory' do
      expect(build(:board)).to be_valid
    end
  end

  describe 'has users' do
    it 'that can be added' do
      user = create(:user)
      board = create(:board)
      expect do
        board.users << user
      end.to change(board.users, :count).by(1)
      expect(board.users.pluck(:id)).to include user.id
    end
  end

  describe 'has_boardable' do
    it 'can be attached to a program' do
      program = create(:program)
      board = create(:board)
      program.boards << board
      expect(program.boards.first.id).to be board.id
      expect(board.boardable.id).to be program.id
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExternalLink, type: :model do
  describe 'validation' do
    it 'should have valid factory' do
      expect(build(:external_link)).to be_valid
    end
  end
end

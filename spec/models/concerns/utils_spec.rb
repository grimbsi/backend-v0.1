# frozen_string_literal: true

require 'rails_helper'

# challenge.address, challenge.city, challenge.country
# community.address, community.city, community.country
#      need.address,      need.city,      need.country
#   project.address,   project.city,   project.country
#      user.address,      user.city,      user.country, user.ip

RSpec.describe Utils, type: :concern do
  describe '#make_address' do
    it 'builds an address string' do
      user = build(:user, address: '123 West Brown St', city: 'Chicago', country: 'U.S.A.')
      expect(user.make_address).to eq('123 West Brown St, Chicago, U.S.A.')
    end
  end

  # only challenge, community, program, project
  # TODO: move out of Utils
  describe '#sanitize_description' do
    it 'strips harmful tags' do
      program = build(:program, description: '<script>wheee</script>')
      program.sanitize_description
      expect(program.description).to eq('wheee')
    end
  end

  # only comment and post
  # TODO: move out of Utils
  describe '#sanitize_content' do
    it 'strips harmful tags' do
      comment = build(:comment, content: '<script>wheee</script>')
      comment.sanitize_content
      expect(comment.content).to eq('wheee')
    end
  end

  # Super seeded by new tests in the challenge and program controllers

  # only challenge, community, program, project
  # TODO: move out of Utils
  # describe '#banner_url_sm' do
  #   it 'uses banner_url if no banner attachment present' do
  #     challenge = build(:challenge, banner_url: 'http://example.com/test.jpg')
  #     banner = double(:banner)
  #     allow(banner).to receive(:attachment) { nil }
  #     allow(challenge).to receive(:banner) { banner }
  #     expect(challenge.banner_url_sm).to eq(challenge.banner_url)
  #   end
  #
  #   # I'm not super stoked about this test but it's passing and now I can refactor
  #   it 'uses something else if banner present' do
  #     challenge = build(:challenge, banner_url: 'http://example.com/test.jpg')
  #     banner = double(:banner)
  #     allow(banner).to receive(:attachment) { 'something' }
  #     variant = double(:variant)
  #     allow(Rails).to receive_message_chain(:application, :routes, :url_helpers, :rails_representation_url).with(variant).and_return('whatever')
  #     allow(banner).to receive(:variant) { variant }
  #     allow(challenge).to receive(:banner) { banner }
  #     expect(challenge.banner_url_sm).to eq('whatever')
  #   end
  # end

  # only works for models with resourcify
  # TODO: move out of utils - doesn't apply to User
  describe '#members_count' do
    it 'returns a count of resource members' do
      challenge = create(:challenge)
      expect(challenge.members_count).to eq(1)
    end
  end

  # used on project, challenge, community, need, program
  # TODO: move out of utils - doesn't apply to User
  describe '#users_sm' do
    it 'returns empty array if no owners, members, or admins present' do
      project = build(:project)
      expect(project.users_sm).to be_empty
    end

    it 'serializes the user' do
      project = create(:project)
      project.roles.destroy_all # clear factory created members
      user = create(:user,
                    id: 10,
                    first_name: 'Yellow',
                    last_name: 'Banana',
                    short_bio: 'Delicious')
      user.add_role(:admin, project)
      user.add_role(:owner, project)
      user.add_role(:member, project)

      expect(project.users_sm.length).to eq(1)
      expect(project.users_sm.first).to include(:id)
      expect(project.users_sm.first).to include(:first_name)
      expect(project.users_sm.first).to include(:last_name)
      expect(project.users_sm.first).to include(:short_bio)
      expect(project.users_sm.first).to include(:owner)
      expect(project.users_sm.first).to include(:admin)
      expect(project.users_sm.first).to include(:member)
      expect(project.users_sm.first).to include(:logo_url)
    end

    it 'returns a maximum of 6 owners' do
      project = create(:project)
      user_list = create_list(:user, 7)
      user_list.each do |user|
        user.add_role(:owner, project)
      end

      expect(project.users_sm.length).to eq(6)
    end

    it 'returns resource owners, then resource admins, then site admins' do
      project = create(:project)
      project.roles.destroy_all # clear factory created members
      user_list = create_list(:user, 5)
      user_list[0..1].each do |user|
        user.add_role(:member, project)
      end
      user_list[2..3].each do |user|
        user.add_role(:admin, project)
        user.add_role(:owner, project)
      end
      admin = user_list.last
      admin.add_role(:admin)

      expect(project.users_sm.length).to eq(4)
      expect(project.users_sm.map { |u| u[:id] }).to eq(
        [user_list[2].id, user_list[3].id, user_list[0].id, user_list[1].id]
      )
    end

    it 'returns 0 system admins if there are no users for resource' do
      project = create(:project)
      project.roles.destroy_all # clear factory created members
      user_list = create_list(:user, 10)
      user_list.each do |user|
        user.add_role(:admin)
      end

      expect(project.users_sm.length).to eq(0)
    end

    it 'shows the permissions of duplicate users as one user' do
      project = create(:project)
      project.roles.destroy_all # clear factory created members
      user = create(:user)
      user.add_role(:admin, project)
      user.add_role(:member, project)
      user.add_role(:owner, project)

      expect(project.users_sm.length).to eq(1)
      expect(project.users_sm.first[:owner]).to eq(true)
      expect(project.users_sm.first[:admin]).to eq(true)
      expect(project.users_sm.first[:member]).to eq(true)
    end
  end

  describe '#reindex(_user)' do
    it 'reindexes a resource as a callback when a user is added/removed' do
      program = create(:program)
      user = create(:user)
      # allow(program).to receive(:reindex).with(user)
      # allow(program).to receive(:persisted?)
      expect(program).to receive(:index!).twice
      program.users << user
      program.users.delete(user)
    end
  end
end
